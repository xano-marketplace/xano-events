# Xano Events

This is a demo of the xano events template.

### Setup & Dependencies
This boilerplate is intended to be as light as possible so outside of the core 
Angular packages only the following are included:

#### List of Dependencies
* material
* bootstrap css
* lodash-es

#### Setup

You can use either npm or yarn to install dependencies.

run `npm install` or `yarn`
