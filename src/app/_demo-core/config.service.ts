import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {XanoService} from "./xano.service";
import {ApiService} from "./api.service";
import {get} from 'lodash-es';
import {MatSnackBar} from "@angular/material/snack-bar";

export interface XanoConfig {
	title: string,
	summary: string,
	editText: string,
	editLink: string,
	descriptionHtml: string,
	components: any,
	instructions: any;
	logoHtml: string,
	requiredApiPaths: string[]
}

export interface Arg {
	tag?: string,
	value?: any
}

export interface Filters {
	name?: string,
	arg?: Arg[]
}

export interface Left {
	operand: string,
	tag: string,
	filters?: Filters[]
}

export interface Right {
	operand: string,
	tag: string,
	filters?: Filters[],
	ignore_empty?: boolean
}

export interface Expression {
	operator?: string,
	left: Left,
	right?: Right
	or?: boolean
}

export interface ColumnSearchExpression {
	operator?: string,
	column: string,
	query: string | boolean,
	or?: boolean
}

export interface GeoSearchExpression {
	column: string,
	lng: number,
	lat: number,
	radius: number,
	or?: boolean
}

@Injectable({
	providedIn: 'root'
})


export class ConfigService {
	public xanoApiUrl: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public newEvent: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public searchQuery: BehaviorSubject<any> = new BehaviorSubject<any>(null);

	public config: XanoConfig = {
		title: 'Events',
		summary: 'Event Template Demo',
		editText: 'Get source code',
		editLink: 'https://gitlab.com/xano-marketplace/xano-events',
		components: [
			{
				name: 'Events',
				description: `This card view orders the events by most recent; it has infinite scroll paging so 
				that as you scroll down, you will load more events without needing to click the next button. You can
				 also use search to search the name of an event. Additionally, it could easily be extended to sort 
				 events by location or filter by category (e.g. concerts, dinners, etc.).`
			},
			{
				name: 'Manage',
				description: 'This is where you can post new events with a cover photo, name, location, and description.'
			},
			{
				name: 'Event View',
				description: 'Here you can view the entire event object with the description.'
			},
			{
				name: 'RSVP',
				description: 'This panel allows people to RSVP to an event using their name and email address.'
			},
		],
		instructions: [
			'Install the template in your Xano Workspace',
			'Go to the newly added Events CRUD API Group and copy your API BASE URL',
			'Paste this in as \'Your Xano API URL\''
		],
		descriptionHtml: ``,
		logoHtml: '',
		requiredApiPaths: [
			'/event',
			'/event/{event_id}',
			'/rsvp',
			'/rsvp/{rsvp_id}',
			'/upload/image',
		]
	};

	constructor(
		private apiService: ApiService,
		private xanoService: XanoService,
		private snackBar: MatSnackBar) {
	}

	public isConfigured(): Observable<any> {
		return this.xanoApiUrl.asObservable();
	}

	public configGet(apiUrl): Observable<any> {
		return this.apiService.get({
			endpoint: this.xanoService.getApiSpecUrl(apiUrl),
			headers: {
				Accept: 'text/yaml'
			},
			responseType: 'text',
		});
	}

	public showErrorSnack(error) {
		this.snackBar.open(get(error, 'error.message', 'An error has occurred'), 'Error', {panelClass: 'error-snack'});
	}

	public searchExpressionBuilder(expressions: Expression[], asGroup: boolean, orGroup: boolean) {
		const statements = [];

		for (let expression of expressions) {
			let statement = {
				statement: {
					left: {
						tag: expression.left.tag,
						operand: expression.left.operand
					},
					right: {
						operand: expression.right.operand
					}
				}
			}

			if (expression.left.filters?.length) {
				Object.assign(statement.statement.left, {filters: expression.left.filters})
			}

			if (expression.operator) {
				Object.assign(statement.statement, {op: expression.operator})
			}

			if (statements.length && expression.or) {
				Object.assign(statement, {or: true})
			}

			if (expression.right.tag) {
				Object.assign(statement.statement.right, {tag: expression.right.tag})
			}

			if (expression.right.filters?.length) {
				Object.assign(statement.statement.right, {filters: expression.right.filters})
			}

			if (expression.right.ignore_empty) {
				Object.assign(statement.statement.right, {ignore_empty: expression.right.ignore_empty})
			}

			statements.push(statement)
		}

		if (asGroup) {
			return {
				type: 'group',
				or: orGroup,
				group: {
					expression: statements
				}
			}
		} else {
			return statements;
		}
	}


	public searchColumn(columnSearchExpressions: ColumnSearchExpression[], asGroup = false, orGroup = false) {
		let expressions = [];

		for (let expression of columnSearchExpressions) {
			expressions.push({
				left: {
					tag: 'col',
					operand: expression.column
				},
				operator: expression.operator,
				right: {
					operand: expression.query
				},
				or: expression.or
			});
		}
		return this.searchExpressionBuilder(expressions, asGroup, orGroup);
	}

	public geoLocationSearch(geoSearchExpressions: GeoSearchExpression[], asGroup=false, orGroup = false) {
		let expressions = []

		for (let expression of geoSearchExpressions) {
			expressions.push({
				or: expression.or,
				left: {
					tag: 'col',
					operand: expression.column,
					filters: [{
						name: 'within',
						arg: [
							{
								tag: 'const',
								value: {
									type: 'point',
									data: {
										lat: expression.lat,
										lng: expression.lng
									}
								}
							}, {
								tag: 'const',
								value: expression.radius
							}
						]
					},
					]
				},
				right: {
					operand: true
				}
			});
		}
		return this.searchExpressionBuilder(expressions, asGroup, orGroup);
	}

}

