import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DemoLandingComponent} from "./demo-landing/demo-landing.component";
import {SafeHtmlPipe} from "./safe-html.pipe";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatIconModule} from "@angular/material/icon";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatDividerModule} from "@angular/material/divider";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";


@NgModule({
	declarations: [
	    DemoLandingComponent,
        SafeHtmlPipe
    ],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		MatIconModule,
		MatFormFieldModule,
		MatExpansionModule,
		MatDividerModule,
		MatCardModule,
		MatButtonModule,
		MatInputModule,
	]
})
export class DemoCoreModule {
}
