import {Component, OnInit} from '@angular/core';
import {EventService} from "../api-services/event.service";
import {get} from 'lodash-es';
import {MatSnackBar} from "@angular/material/snack-bar";
import {cities} from "../../cities";
import {ConfigService} from "../_demo-core/config.service";
import {MatDialog} from "@angular/material/dialog";
import {RsvpPanelComponent} from "../rsvp-panel/rsvp-panel.component";
import {ViewEventPanelComponent} from "../view-event-panel/view-event-panel.component";
import {finalize, switchMap} from "rxjs/operators";

@Component({
	selector: 'app-events',
	templateUrl: './events.component.html',
	styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
	public events: any;
	public cities = cities;
	public noResults: boolean;
	public loading: boolean = true;

	public hasNextPage: boolean;
	public currentPage: number = 1;
	public _location: any;
	public dateFilter: any;
	public today: Date = new Date()
	public date = new Date();
	public tomorrow = new Date();
	public lastDayOfWeek: Date = new Date(this.date.setDate(this.date.getDate()- this.date.getDay() + 6));
	public searchValue: string;

	constructor(
		private configService: ConfigService,
		private eventService: EventService,
		private snackBar: MatSnackBar,
		private dialog: MatDialog
	) {
	}

	ngOnInit(): void {
		this.today.setHours(0,0,0,0);
		this.lastDayOfWeek.setHours(0,0,0,0);
		this.tomorrow.setDate(this.today.getDate() + 1);
		this.tomorrow.setHours(0,0,0,0);

		this.getEvents(null, null).subscribe(res => {
			this.events = res?.items;
			this.currentPage = res?.curPage;
			this.hasNextPage = res?.nextPage;
		}, error => this.configService.showErrorSnack(error));

		this.configService.searchQuery.asObservable()
			.pipe(switchMap((value)=> {
				this.searchValue = value;
				return this.getEvents(null, this._location)
			}))
			.subscribe(res => {
				this.currentPage = res?.curPage;
				this.hasNextPage = res?.nextPage;
				this.events = res?.items;
		}, error => this.configService.showErrorSnack(error));


		this.configService.newEvent.asObservable().subscribe(res => {
			if (res) {
				this.events.unshift(res);
			}
		});
	}

	public getLocation(lat, lng): string {
		let location = this.cities.find(x => x.latitude == lat && x.longitude == lng);

		if (location) {
			return location.city + ', ' + location.state;
		} else {
			return '';
		}
	}

	public showPanel(dialogType, event): void {
		let dialogRef;
		switch (dialogType) {
			case 'view':
				dialogRef = this.dialog.open(ViewEventPanelComponent, {data: {event: event}});
				break;
			case 'rsvp':
				dialogRef = this.dialog.open(RsvpPanelComponent, {data: {event: event}});
				dialogRef.afterClosed().subscribe(res => {
					if(res?.rsvp) {
						this.events.map(x => {
							if(x.id === res.event_id) {
								x.rsvp_count =  x.rsvp_count + 1;
							}
							return x;
						})
					}
				})
				break;
			default:
				break;
		}
	}

	public getEvents(page, location) {
		const search = {
			page: page,
			expression: []
		};

		if(location) {
			search.expression.push(location);
		}

		if(this.dateFilter) {
			search.expression = [...search.expression, this.dateFilter]
		}

		if (this.searchValue) {
			this.searchValue.trim();
			const expression = this.configService.searchColumn([
				{operator: 'includes', query: this.searchValue, column: 'event.name'},
				{operator: 'includes', query: this.searchValue, column: 'event.description', or: true},
			]);
			// @ts-ignore
			search.expression = [...search.expression, ...expression]
		}
		this.loading = true;
		return this.eventService.getEvents(search).pipe(finalize(()=>  this.loading = false));
	}

	public loadMore() {
		if (this.hasNextPage) {
			this.getEvents(this.currentPage + 1, this._location).subscribe(res => {
				if (res["itemsReceived"] > 0) {
					this.currentPage = res["curPage"];
					this.hasNextPage = !!res["nextPage"]
					this.events = [...this.events, ...res["items"]];
				}
			});
		}
	}

	public toggleNearMe(event) {
		if (event.checked) {
			this.events = [];
			this.loading = true;
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition((position) => {
					this._location = this.configService.geoLocationSearch([{
						column: 'event.location',
						lat: position.coords.latitude,
						lng: position.coords.longitude,
						radius: 40000,
					}])[0];
					this.getEvents(null, this._location).subscribe(res => {
						this.currentPage = res["curPage"];
						this.hasNextPage = !!res["nextPage"]
						this.events = res.items;
					});
				});
			}
		} else {
			this._location = null;
			this.getEvents(null, this._location).subscribe(res => {
				this.currentPage = res["curPage"];
				this.hasNextPage = !!res["nextPage"]
				this.events = res?.items;
			});
		}
	}

	public filterDate(filterBy): void {
		switch (filterBy) {
			case 'all':
				this.dateFilter = null;
				break;
			case 'week':
				this.dateFilter = this.configService.searchColumn([
					{column: 'event.start_at', query: Date.parse(this.today.toString()).toString(), operator: '>='},
					{column:'event.start_at', query: Date.parse(this.lastDayOfWeek.toString()).toString(), operator: '<='},
				], true)
				break;
			case 'today':
				this.dateFilter = this.configService.searchColumn([
					{column: 'event.start_at', query: Date.parse(this.today.toString()).toString(), operator: '>='},
					{column:'event.start_at', query: Date.parse(this.tomorrow.toString()).toString(), operator: '<='},
				], true)
				break;
			default: break;
		}
		this.getEvents(null, this._location).subscribe(res => {
			this.currentPage = res["curPage"];
			this.hasNextPage = !!res["nextPage"]
			this.events = res.items;
		})
	}
}
