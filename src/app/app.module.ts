import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {HomeComponent} from './home/home.component';
import {HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from "@angular/material/button";``
import {ManagePanelComponent} from './manage-panel/manage-panel.component';
import {MatCardModule} from "@angular/material/card";
import {MatInputModule} from "@angular/material/input";
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from "@angular/material/dialog";
import {ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule} from "@angular/material/snack-bar";
import {MatCommonModule, MatOptionModule} from "@angular/material/core";
import {QuillModule} from "ngx-quill";
import {MatSelectModule} from "@angular/material/select";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {NgxMatDatetimePickerModule, NgxMatNativeDateAdapter} from "@angular-material-components/datetime-picker";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {NgxMatMomentAdapter, NgxMatMomentModule} from "@angular-material-components/moment-adapter";
import {EventsComponent} from './events/events.component';
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {ViewEventPanelComponent} from './view-event-panel/view-event-panel.component';
import {RsvpPanelComponent} from './rsvp-panel/rsvp-panel.component';
import {InfiniteScrollModule} from "ngx-infinite-scroll";
import {DemoCoreModule} from "./_demo-core/demo-core.module";
import {AppRouterOutletComponent} from "./app-router-outlet/app-router-outlet.component";
import {MatExpansionModule} from "@angular/material/expansion";

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		HeaderComponent,
		AppRouterOutletComponent,
		ManagePanelComponent,
		EventsComponent,
		ViewEventPanelComponent,
		RsvpPanelComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		MatCommonModule,
		HttpClientModule,
		BrowserAnimationsModule,
		ReactiveFormsModule,
		DemoCoreModule,
		MatIconModule,
		MatExpansionModule,
		MatButtonModule,
		MatSnackBarModule,
		MatCardModule,
		MatInputModule,
		MatDialogModule,
		QuillModule.forRoot(),
		MatOptionModule,
		MatSelectModule,
		MatSlideToggleModule,
		MatProgressSpinnerModule,
		NgxMatDatetimePickerModule,
		NgxMatMomentModule,
		MatDatepickerModule,
		MatButtonToggleModule,
		InfiniteScrollModule
	],
	providers: [{
		provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
		useValue: {
			duration: 4000,
			horizontalPosition: 'start'
		}
	}, {
		provide: MAT_DIALOG_DEFAULT_OPTIONS,
		useValue: {
			hasBackdrop: true,
			minHeight: '100vh',
			position: {top: '0px', right: '0px'},
			panelClass: 'slide-out-panel'
		},
	}, {
		provide: NgxMatMomentAdapter,
		useClass: NgxMatNativeDateAdapter
	}],
	bootstrap: [AppComponent]
})
export class AppModule {
}
