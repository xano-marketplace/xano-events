import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {EventService} from "../api-services/event.service";
import {cities} from "../../cities";
import {MatSnackBar} from "@angular/material/snack-bar";
import {get} from 'lodash-es';

@Component({
	selector: 'app-rsvp-panel',
	templateUrl: './rsvp-panel.component.html',
	styleUrls: ['./rsvp-panel.component.scss']
})
export class RsvpPanelComponent implements OnInit {

	public cities: any = cities;
	private emailRegex = new RegExp('^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$');
	public rsvpForm: FormGroup = new FormGroup({
		event_id: new FormControl('', Validators.required),
		name: new FormControl('', Validators.required),
		email: new FormControl('', [Validators.required, Validators.pattern(this.emailRegex)]),

	});

	constructor(
		@Inject(MAT_DIALOG_DATA) public data,
		private dialogRef: MatDialogRef<RsvpPanelComponent>,
		private eventService: EventService,
		private snackBar: MatSnackBar
	) {
	}

	ngOnInit(): void {
		if(this.data?.event) {
			this.rsvpForm.controls.event_id.patchValue(this.data.event.id)
		} else {
			this.dialogRef.close();
		}
	}

	public submit(): void {
		this.rsvpForm.markAllAsTouched()

		if(this.rsvpForm.valid) {
			this.eventService.rsvp(this.rsvpForm.getRawValue()).subscribe(res => {
				if(res) {
					const message = `You RSVPed to ${this.data.event.name}`
					this.snackBar.open(message, 'Success')
					this.dialogRef.close({rsvp: true, event_id: this.rsvpForm.controls.event_id.value})
				}
			}, error => {
				this.snackBar.open(
					get(error, 'error.message', 'An Error Occurred'),
					'Error',
					{panelClass: 'error-snack'}
				)
			});
		}

	}

	public getLocation(lat, lng): string {
		let location = this.cities.find(x => x.latitude == lat && x.longitude == lng);

		if (location) {
			return location.city + ', ' + location.state;
		} else {
			return '';
		}
	}

}
