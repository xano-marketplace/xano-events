import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {ConfigService} from "../_demo-core/config.service";
import {ApiService} from "../_demo-core/api.service";

@Injectable({
	providedIn: 'root'
})
export class EventService {

	constructor(private configService: ConfigService, private apiService: ApiService) {
	}

	public getEvents(search): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/event`,
			params: {search}
		});
	}

	public getEvent(eventID: number): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/event/${eventID}`,
		});
	}

	public saveEvent(event: any): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/event`,
			params: event,
		});
	}

	public updateEvent(event): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/event/${event.id}`,
			params: event,
		});
	}

	public deleteEvent(eventID: number): Observable<any> {
		return this.apiService.delete({
			endpoint: `${this.configService.xanoApiUrl.value}/event/${eventID}`,
		});
	}

	public upload(image): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/upload/image`,
			params: image
		});
	}

	public rsvp(rsvp): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/rsvp`,
			params: rsvp,
		});
	}

}
