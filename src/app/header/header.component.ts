import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from '../_demo-core/config.service';
import {MatDialog} from '@angular/material/dialog';
import {ManagePanelComponent} from "../manage-panel/manage-panel.component";
import {FormControl} from "@angular/forms";
import {debounceTime, distinctUntilChanged, switchMap} from "rxjs/operators";

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
	public apiConfigured: boolean = false;
	public config: XanoConfig;
	public search: FormControl = new FormControl('');

	constructor(
		private configService: ConfigService,
		private dialog: MatDialog
	) {
		this.config = configService.config;
	}

	ngOnInit(): void {
		this.configService.isConfigured().subscribe(apiUrl => {
			this.apiConfigured = !!apiUrl;
		});


		this.search.valueChanges.pipe(debounceTime(300), distinctUntilChanged()
		).subscribe(res => {
			this.configService.searchQuery.next(res);
		})
	}


	public manage() {
		const dialogRef = this.dialog.open(ManagePanelComponent);
		dialogRef.afterClosed().subscribe(res => {
			if (res?.new) {
				this.configService.newEvent.next(res?.item)
			}
		})
	}

	public resetSearch() {
		this.search.reset();
	}


}
