import {Component, Inject, OnInit} from '@angular/core';
import {MatSnackBar} from "@angular/material/snack-bar";
import {EventService} from "../api-services/event.service";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {get} from 'lodash-es';
import {cities} from "../../cities";
import {RsvpPanelComponent} from "../rsvp-panel/rsvp-panel.component";

@Component({
	selector: 'app-view-event-panel',
	templateUrl: './view-event-panel.component.html',
	styleUrls: ['./view-event-panel.component.scss']
})
export class ViewEventPanelComponent implements OnInit {
	public event: any;
	public cities: any =  cities;

	constructor(
		private eventService: EventService,
		private snackbar: MatSnackBar,
		private dialog: MatDialog,
		private dialogRef: MatDialogRef<ViewEventPanelComponent>,
		@Inject(MAT_DIALOG_DATA) public data
	) {
	}

	ngOnInit(): void {
		if(this.data?.event) {
			this.eventService.getEvent(this.data.event.id).subscribe(res => {
				this.event = res;
			}, error =>  {
				this.snackbar.open(
					get(error, 'error.message', 'An Error Occurred'),
					'Error',
					{panelClass: 'error-snack'}
				);
				this.dialogRef.close();
			})
		}
	}

	public getLocation(lat, lng): string {
		let location = this.cities.find(x => x.latitude == lat && x.longitude == lng);

		if (location) {
			return location.city + ', ' + location.state;
		} else {
			return '';
		}
	}

	rsvp() {
		const dialogRef = this.dialog.open(RsvpPanelComponent, {data: {event: this.event}});

		dialogRef.afterClosed().subscribe(res => {
			if(res?.rsvp) {
				this.event.rsvp_count = this.event.rsvp_count + 1;
			}
		})
	}
}
