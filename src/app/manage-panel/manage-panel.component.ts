import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {EventService} from "../api-services/event.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {cities} from "../../cities";
import {MatSnackBar} from "@angular/material/snack-bar";
import {finalize} from "rxjs/operators";
import {get} from 'lodash-es';

@Component({
	selector: 'app-manage-panel',
	templateUrl: './manage-panel.component.html',
	styleUrls: ['./manage-panel.component.scss']
})
export class ManagePanelComponent implements OnInit {

	public eventForm: FormGroup = new FormGroup({
		id: new FormControl(null),
		name: new FormControl('', Validators.required),
		location: new FormControl(),
		is_online_event: new FormControl(false),
		online_event_link: new FormControl(''),
		description: new FormControl(''),
		cover_photo: new FormControl(null),
		start_at: new FormControl(null, Validators.required),
		end_at: new FormControl(null),
	});

	public location: any;
	public cities = cities;
	public uploading: boolean;
	public image: any;
	public startOpen: boolean;

	public minDate = new Date();

	public modules = {
		toolbar: [
			['bold', 'italic', 'underline'],
			['blockquote'],
			[{header: 1}, {header: 2}],
			[{list: 'ordered'}, {list: 'bullet'}],
			[{color: []}],
			['link']
		]
	};


	constructor(
		private eventService: EventService,
		private dialogRef: MatDialogRef<ManagePanelComponent>,
		private snackbar: MatSnackBar,
		@Inject(MAT_DIALOG_DATA) public data
	) {
	}

	ngOnInit(): void {
	}

	public submit(): void {
		this.eventForm.markAllAsTouched();

		if (this.eventForm.valid) {
			if (this.eventForm.controls.start_at.value?._d) {
				this.eventForm.controls.start_at.patchValue(this.eventForm.controls.start_at.value._d);
			}

			if (this.eventForm.controls.end_at.value?._d) {
				this.eventForm.controls.end_at.patchValue(this.eventForm.controls.end_at.value._d);
			}

			if(this.eventForm.controls.is_online_event.value) {
				this.eventForm.controls.location.patchValue(null)
			} else {
				this.eventForm.controls.online_event_link.patchValue(null)

				const location = this.eventForm.controls.location;
				if (!location.value?.data) {
					location.patchValue({
						type: 'point',
						data: {lat: location.value.latitude, lng: location.value.longitude}
					})
				}
			}

			if (this.data?.event) {

			} else {
				this.eventService.saveEvent(this.eventForm.getRawValue()).subscribe(res => {
					this.snackbar.open('Event', 'Created');
					this.dialogRef.close({new: true, item: res})
				}, error => {
					this.snackbar.open(
						get(error, 'error.message', 'An Error Occurred'),
						'Error',
						{panelClass: 'error-snack'}
					)
				})
			}
		}
	}

	upload(event) {
		this.uploading = true;
		const file: File = event.target.files[0];
		const formData: FormData = new FormData();
		formData.append('content', file, file.name);
		this.eventService.upload(formData)
			.pipe(
				finalize(() => this.uploading = false)
			)
			.subscribe(res => {
				this.eventForm.controls.cover_photo.patchValue(res);
				const reader = new FileReader();
				reader.onload = () => this.image = reader.result;
				reader.readAsDataURL(file);
			}, error => this.snackbar.open('Image Upload', 'Failed', {panelClass: 'error-snack'}));
	}

	public deleteImage() {
		this.eventForm.controls.cover_photo.patchValue(null);
	}

}
