import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {AppRouterOutletComponent} from "./app-router-outlet/app-router-outlet.component";
import {DemoLandingComponent} from "./_demo-core/demo-landing/demo-landing.component";
import {ConfigGuard} from "./_demo-core/config.guard";

const routes: Routes = [
	{path: '', component: DemoLandingComponent, data: {animation: 'demoLandingPage'}},
	{
		path: 'app',
		component: AppRouterOutletComponent,
		canActivate: [ConfigGuard],
		children: [
			{path: '', component: HomeComponent},
		]
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
